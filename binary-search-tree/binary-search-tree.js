export default class BinarySearchTree {

    constructor (data) {
        this.data = data
        this.left = undefined
        this.right = undefined
    }

    insertLeft (data) {
        if (!this.left) {
            this.left = new BinarySearchTree(data)
        } else {
            this.left.insert(data)
        }
    }

    insertRight (data) {
        if (!this.right) {
            this.right = new BinarySearchTree(data)
        } else {
            this.right.insert(data)
        }
    }

    insert (data) {
        if (data <= this.data ) {
            this.insertLeft(data)
        } else {
            this.insertRight(data)
        }
    }

    each (fn) {
        if (this.left) {
            this.left.each(fn)
        }
        fn(this.data)
        if (this.right) {
            this.right.each(fn)
        }
    }
}