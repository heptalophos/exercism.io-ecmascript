const THINGS = [ 'house', 'malt', 'rat', 'cat', 'dog',
                 'cow with the crumpled horn',
                 'maiden all forlorn',
                 'man all tattered and torn',
                 'priest all shaven and shorn',
                 'rooster that crowed in the morn',
                 'farmer sowing his corn',
                 'horse and the hound and the horn' ]

const ACTIONS = [ 'Jack built', 'lay in', 'ate', 'killed', 'worried',
                  'tossed', 'milked', 'kissed', 'married', 'woke',
                  'kept', 'belonged to' ]


export default class House {
    
    static verse (num) {
        const lines = []
        let idx = num - 1
        for (let i = 1; i <= num; i += 1) {
            let line = ''
            if ( i === 1 ) line += 'This is'
            else {
                line += `that ${ACTIONS[idx]}`
                idx -= 1
            }
            line += ` the ${THINGS[idx]}`
            if ( i === num ) line += ` that ${ACTIONS[idx]}.`
            lines.push(line)
        }
        return lines
    }

    static verses (start, end) {
        let lines = []
        for (let i = start; i <= end; i += 1) {
            const lyrics = House.verse(i)
            lines = lines.concat(lyrics)
            if (i < end) lines.push('')
        }
        return lines
    }
}