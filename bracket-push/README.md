# Bracket Push

Given a string containing brackets `[]`, braces `{}` and parentheses `()`,
verify that all the pairs are matched and nested correctly.

## Setup

Go through the setup instructions for ECMAScript to
install the necessary dependencies:

http://exercism.io/languages/ecmascript

## Requirements

Install assignment dependencies:

```bash
$ npm install
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```

In the test suites all tests but the first have been skipped.

Once you get a test passing, you can enable the next one by
changing `xtest` to `test`.

## Alternative Testing

Execute the tests with:

```bash
$ jasmine bracket-push.spec.js
```
Change `test` in spec.js  file to  `it` 
In the test suites all tests but the first have been skipped.

Once you get a test passing, you can enable the next one by
changing `xit` to `it`.

## Source

Ginna Baker

## Submitting Incomplete Solutions
It's possible to submit an incomplete solution so you can see how others have completed the exercise.
