const trimTrailing = array => 
    array.slice(0, array.length - [...array].reverse().findIndex(x => x !== undefined))

export default function transpose (input) {
    const maxCol = Math.max(0, ...(input.map(row => row.length)))
    return [...Array(maxCol).keys()]
           .map (col => trimTrailing(input.map ((_, row) => input[row][col]))
           .map (ch => ch || ' ')
           .join(''))
}