export default class Flattener {
    flatten(array) {
        let flat = []
        array.forEach(element => {
        if (!Array.isArray(element)) {
            if (element !== null && element !== undefined)
                flat.push(element)
        } else {
            this.flatten(element).forEach(elem => flat.push(elem))
        }
        })
        return flat
    }
}

