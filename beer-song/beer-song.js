
const bottles = (num) => (num === 0) ? "No more bottles" : (num === 1) ? "1 bottle" : `${num} bottles`

// const w = (v) => ( v === 1 ) ? "it" : "one"

const action = (curr) => (curr === 0) ? "Go to the store and buy some more, " : `Take ${(curr === 1) ? 'it' : 'one'} down and pass it around, ` 


const next = (curr) => (curr === 0) ? 99 : curr - 1


export default class Beer {

    constructor () {}

    static verse (num) {
        const s0 = `${bottles(num)} of beer on the wall, `
        const s1 = `${bottles(num).toLowerCase()} of beer.\n`
        const s2 = action(num)
        const s3 = `${bottles(next(num)).toLowerCase()} of beer on the wall.\n`

        return [s0, s1, s2, s3].join('')
    }

    static sing (bmax=99, bmin=0) {
        return Array.from({length: bmax - bmin + 1}, (_, i) => Beer.verse(bmax - i)).join('\n')
    }
}

