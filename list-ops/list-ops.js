export default class List {

    constructor (list) {

        this.values = list || []
    }

    append (list2) {
        if (list2 === []) {
            return this
        }
        for (const v of list2.values) {
            this.values.push(v)
        }
        return this
    }

    concat (list2) {
        return this.append(list2)
    }

    filter (pred) {
        const filtered = []
        for (const v of this.values) {
            if (pred(v)) {
                filtered.push(v)
            }
        }
        this.values = filtered
        return this
    }

    length () {
        return this.values.length
    }

    map (fcn) {
        const mapped = []
        for (const v of this.values) {
            mapped.push(fcn(v))
        }
        this.values = mapped
        return this
    }

    foldl (fcn, init) {
        let ac = init
        for (const v of this.values) {
            ac = fcn(ac, v)
        }
        return ac
    }

    foldr (fcn, init) {
        let ac = init
        let i = this.length() - 1
        while (i >= 0) {
            const v = this.values[i--]
            ac = fcn(ac, v)
        }
        return ac
    }

    reverse () {
        const l = this.length()
        let last = l - 1
        for (let i = 0; i < l/2; i++) {
            const tmp = this.values[i]
            this.values[i] = this.values[last]
            this.values[last--] = tmp
        }
        return this
    }
}