const factors = (number) => { 
    return [...Array(Math.floor(number / 2) + 1).keys()].slice(1).filter(i => number % i === 0) 
}

const aliquotClass = (num, aliquot) => (num === 1 || aliquot < num) 
                                     ? "deficient" 
                                     : (aliquot === num) 
                                     ? "perfect" : "abundant"

export default class PerfectNumbers {

    classify (number) {
        if (number <= 0) throw Error('Classification is only possible for natural numbers.')
        const sum = factors(number).reduce((a, b) => a + b, 0)
        return aliquotClass(number, sum)
      }
}