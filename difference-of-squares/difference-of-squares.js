export default class Squares {

    constructor (n) {
        this.squareOfSums = (n * (n + 1) / 2) ** 2
        this.sumOfSquares = (n ** 3) / 3 + (n ** 2) / 2 + n / 6 
        this.difference   = this.squareOfSums - this.sumOfSquares
    }
}
