const isPalindrome = number => [...number.toString()].reverse().join('') === number.toString()

const generate = ({minFactor=1, maxFactor}) => {
    let minProduct = Infinity
    let maxProduct = 1
    const data = {}
    for (let f1 = minFactor; f1 <= maxFactor; f1++) {
        for (let f2 = f1; f2 <= maxFactor; f2++) {
            if (isPalindrome(f1 * f2)) {
                const factors = data[f1 * f2] || []
                factors.push(f1, f2)
                data[f1*f2] = factors
                minProduct = Math.min(minProduct, f1*f2)
                maxProduct = Math.max(maxProduct, f1*f2)
            }
        }
    }
    return {
        largest : {
            value : maxProduct,
            factors : data[maxProduct] 
        },
        smallest : { 
            value : minProduct,
            factors : data[minProduct]
        }
    }

}

export default generate