export default class ComplexNumber {

    constructor (real, imag) {
        this.real = real
        this.imag = imag
    }

    add (cnum) {
        return new ComplexNumber(this.real + cnum.real, this.imag + cnum.imag)
    }

    sub (cnum) {
        return new ComplexNumber(this.real - cnum.real, this.imag - cnum.imag)
    }

    mul (cnum) {
        return new ComplexNumber(this.real * cnum.real - this.imag * cnum.imag, 
                                 this.real * cnum.imag + this.imag * cnum.real)
    }

    div (cnum) {
        const denominator = cnum.real ** 2 + cnum.imag ** 2
        return new ComplexNumber((this.real * cnum.real + this.imag * cnum.imag) / denominator,
                                 (this.imag * cnum.real - this.real * cnum.imag) / denominator)
    }

    get abs () {
        return Math.sqrt(this.real ** 2 + this.imag ** 2)
    }

    get conj () {
        return new ComplexNumber(this.real, 0 - this.imag)
    }

    get exp () {
        return new ComplexNumber(Math.exp(this.real) * Math.cos(this.imag), 
                                 Math.exp(this.real) * Math.sin(this.imag))
    }


}