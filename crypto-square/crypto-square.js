
const squareSize = (str) => Math.ceil(Math.sqrt(str.length))

const chunkify = (input, chunkSize) => {
    const output = []
    let listchars = [...input]
    while (listchars.length > 0) {
        output.push(listchars.splice(0, chunkSize).join(''))    
    }
    return output
}

const cipherText = (square, chunkSize) => {
    let cipher = ''
    for (let i = 0; i < chunkSize; i++) {
        for (let j = 0; j < square.length; j++) {
            cipher += square[j][i] || ""
        }
    }
    return cipher
}

export default class Crypto {

    constructor (text='') {
        this.text = text.toLowerCase()
        this.squareSize = squareSize(this.normalizePlaintext(this.text))
    }

    size () {
        return this.squareSize 
    } 

    ciphertext () {
        return cipherText(this.plaintextSegments(), this.squareSize)
    }

    normalizeCiphertext () {
        return chunkify(this.ciphertext, this.squareSize).join(' ')
    }

    normalizePlaintext () {
        return this.text.replace(/[\W]/g, '')
    }

    plaintextSegments () {
        return chunkify(this.normalizePlaintext(this.text), this.squareSize)
    }
} 