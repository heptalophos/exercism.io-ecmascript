export default class SecretHandshake {

    constructor (num) {
        if (!Number.isInteger(num)) throw new Error ('Handshake must be a number')
        this.number = num
    }

    commands () {
   
        const COMMANDS = [
            [1 << 0, 'wink'],
            [1 << 1, 'double blink'],
            [1 << 2, 'close your eyes'],
            [1 << 3, 'jump']
        ]

        const secret = []
        
        for (const [mask, c] of COMMANDS) { 
            if ((this.number & mask) !== 0) secret.push(c) 
        }
        if ((this.number & 1 << 4) !== 0) secret.reverse()
        
        return secret
    }
}