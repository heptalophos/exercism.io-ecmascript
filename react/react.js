export class InputCell {

    constructor(val) {
        this.value = val;
        this.updated = true;
        this.subscribers = [];
    }

    setValue (val) {
        if (this.value !== val ) {
            this.value = val
            this.notifySubscribers()
        }
    }

    addSubscriber (sub) {
        this.subscribers.push(sub)
    }

    notifySubscribers () {
        this.subscribers.forEach (sub => sub.mark())
        this.subscribers.forEach (sub => sub.update())
    }
}

export class ComputeCell {

    constructor (inputCells, fcn) {
        this.fcn = fcn
        this.inputCells = inputCells
        this.inputCells.forEach (c => c.addSubscriber(this))
        this.subscribers = []
        this.callbacks = []
        this.value = fcn(inputCells)
        this.updated = true
        this.last = this.value
    }

    mark () {
        this.updated = false
        this.subscribers.forEach(sub => sub.mark())
    }

    update () {
        let value = this.fcn(this.inputCells)
        if (value !== this.value) {
            this.value = value
            this.updated = true
            this.notifySubscribers()
        }
    }

    changed () {
        return this.last !== this.value
    }

    allUpdated () {
        return this.inputCells
              .filter(c => c.updated)
              .length === this.inputCells.length
    }

    runCallbacks () {
        if (this.allUpdated() && this.changed()) {
            this.last = this.value
            this.callbacks.forEach (callback => callback.run(this))
        }
    }

    addSubscriber (sub) {
        this.subscribers.push(sub)
    }

    addCallback (callback) {
        this.callbacks.push(callback)
    }

    removeCallback (callback) {
        this.callbacks = this.callbacks.filter(cb => cb !== callback)
    }

    notifySubscribers () {
        this.subscribers.forEach (sub => sub.mark())
        this.subscribers.forEach (sub => sub.update())
        this.runCallbacks()
    }
}

export class CallbackCell {

    constructor (fcn) {
        this.fcn = fcn
        this.values = []
    }

    run (cell) {
        this.values.push(this.fcn(cell))
    }
} 
