
export const encode = (plain) => {
    return plain.replace(/(.)\1+/g, (group, ch) => group.length + ch)
}

export const decode = (encoded) => {
    return encoded.replace(/(\d+)(\w|\s)/g, (match, cnt, ch) => ch.repeat(cnt))
}