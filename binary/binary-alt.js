
// recursive binary to decimal function. Starts from head of list, so argneeds to be reversed 
const bin2dec = ([h, ...t]) => parseInt(h) + (t.length > 0 ? 2 * bin2dec(t) : 0)

export  default class Binary {

    constructor (binary) {

        this.binary = binary.match(/^[01]+$/) ? parseInt(binary, 2) : 0

    }

    toDecimal () {

        return (/^[01]+$/.test(this.binary)) ? 0 : bin2dec(this.binary.toString().split('').reverse())
                
    }
}