const food = (animal, sent, action = '') => ({ animal, sent, action })

const FoodChain = [
    food ('fly', "I don't know why she swallowed the fly. Perhaps she'll die."),
    food ('spider', 'It wriggled and jiggled and tickled inside her.', ' that wriggled and jiggled and tickled inside her'),
    food ('bird', 'How absurd to swallow a bird!'),
    food ('cat', 'Imagine that, to swallow a cat!'),
    food ('dog', 'What a hog, to swallow a dog!'),
    food ('goat', 'Just opened her throat and swallowed a goat!'),
    food ('cow', "I don't know how she swallowed a cow!"),
    food ('horse', "She's dead, of course!")
]

const firstLine = animal => `I know an old lady who swallowed a ${animal}.`


export default class Song {
    
    constructor () {}

    verse (num) {
        const lines = []
        if (num === 8) {
            const {animal, sent} = FoodChain[7]
            lines.push(firstLine(animal))
            lines.push(sent)
        } else {
            const foodchainSlice = FoodChain.slice(0, num).reverse()
            foodchainSlice.forEach(({animal, sent, action}, i) => {
                if (i === 0) {
                    lines.push(firstLine(animal))
                } else {
                    lines.push(`She swallowed the ${foodchainSlice[i-1].animal}` + ` to catch the ${animal}${action}.`)
                }
                if (i === 0 || i === foodchainSlice.length - 1) {
                    lines.push(sent)
                }
            })
        }
        return lines.join('\n') + '\n'
    }

    verses (min=1, max=8) {
        return Array.from({length:max - min + 1}, (_, i) => this.verse (min + i)).join('\n') + '\n'
    }
}