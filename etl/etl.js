const transform = old => {
    let nouveau = {}
    for (let i in old) {
        old[i].forEach (ch => nouveau[ch.toLowerCase()] = Number(i))
    } 
    return nouveau
}

export default transform