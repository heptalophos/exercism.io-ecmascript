const revStrRec = (str, len) => str ? revStrRec(str.substr(1), len - 1) + str[0] : str

export default function reverseString (str) {
    // return (str === '') ? '' : reverseString(str.substr(1)) + str.charAt(0)
    return revStrRec(str, str.length)
} 