const Roman = {
    0: ['','I','II','III','IV','V','VI','VII','VIII','IX'],
    1: ['', 'X', 'XX', 'XXX', 'XL','L','LX','LXX','LXXX','XC'],
    2: ['','C', 'CC','CCC','CD','D','DC','DCC','DCCC','CM'],
    3: ['','M','MM','MMM'] }

const toRoman = (num) => [...num.toString()].reverse().reduce((str, digit, power) => (Roman[power])[digit] + str, '')

export default toRoman
