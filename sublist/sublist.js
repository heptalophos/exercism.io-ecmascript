// const contains = (sup, sub) => {
//     sup === [] 
//     ? false 
//     : sub === [] 
//     ? true 
//     : sub.filter(x => !(x in sup)).length === 0
// }

export default class List {

    constructor (list=[]) {
        this.list = list
    }

    compare (other) {
        if (this.list.length === other.list.length  &&  this.contains(this, other)) {
            return 'EQUAL'
        } else if (this.contains(other, this)) {
            return 'SUBLIST'
        } else if (this.contains(this, other)) {
            return 'SUPERLIST'
        } else {
            return 'UNEQUAL'
        }
    }

    contains (sup, sub) {
        return sub.list.length === 0 || sup.list.some((supElem, supIdx) => 
            sub.list.every((subElem, subIdx) => subElem === sup.list[supIdx + subIdx]))
    }

}