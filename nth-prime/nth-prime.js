const sieve = (upto) => {
    let p = 2
    const range = [], primes = []
    for (let i = 2; i <= upto; i++) 
        range[i] = {val: i, prime: true}
    for (let i = 2; i <= upto / 2; i++) {
        for (let j = 2 * p; j <= upto; j += p) {
            range[j].prime = false
        }
        p++
    }
    for (let k in range)
        range[k].prime ? primes.push(+k) : null
    return {
        primes: primes
    }
}

export default class Prime {

    nth (number) {
        if (number < 1) throw new Error("Prime is not possible")
        let approximate = number * Math.log(number) + number * Math.log(Math.log(number))
        approximate < 15 ? approximate = 15 : {}
        return sieve(approximate).primes[number - 1]
    }
}