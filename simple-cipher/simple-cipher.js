
const ALPHA = 'abcdefghijklmnopqrstuvwxyz'

const mod = (n, m) => (n % m + m) % m

const xCode = (key, input, sign) => {
    return [...input].reduce((output, letter, ii) => {
        const offset = sign * ALPHA.indexOf(key[mod(ii, key.length)])
        output += ALPHA[mod(ALPHA.indexOf(letter) + offset, ALPHA.length)]
        return output
    }, '')
}

export default class Cipher{

    constructor(key) {

        if (typeof key === 'undefined') {
            key = this.generateKey();
        } 
        else if (key.length === 0 || key.match(/[^a-z]/, 'g')) {
            throw new Error('Bad key');
        }

        return {
            key,
            encode(plaintxt) {
                return xCode(this.key, plaintxt, 1)
            },
            decode(ciphertxt) {
                return xCode(this.key, ciphertxt, -1)
            }
        }
    }

    generateKey() {
        return Array(...Array(100)).map(() => ALPHA[Math.floor(Math.random() * ALPHA.length)]).join('')
    }

    
} 