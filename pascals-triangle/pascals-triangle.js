
const fact = (n) => n <= 1 ? 1 : n * fact (n - 1) 

const cell = (n, k) => fact (n) / fact (k) / fact (n - k)

const genRows = (size) => Array.from({length: size}, (_, i) => Array.from({length: i+1}, (_, j) => cell(i, j)))

export default class Triangle {

    constructor (size) {
        this.size = size
        this.rows = genRows(size)
        this.lastRow = this.rows[this.rows.length - 1]
    }
 
} 