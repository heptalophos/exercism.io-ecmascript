const GIGASECOND = 1e09 
const MILS = 1e03

export default class Gigasecond {

    constructor (dob) {
        this.dob = dob
    }

    date () {
        return new Date(this.dob.getTime() + MILS * GIGASECOND)
    }
}