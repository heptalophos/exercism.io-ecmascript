const INSTRUCTIONS = { L : 'turnLeft', R : 'turnRight', A : 'advance' }
const DIRECTIONS   = ['north', 'east', 'south', 'west']

export default class RobotSimulator {

    constructor () {
        this.coordinates = [0, 0]
        this.bearing = DIRECTIONS[0]
    }

    orient (direction) {
        if (DIRECTIONS.indexOf(direction) < 0) {
            throw InvalidInputError("Invalid Robot Bearing.")
        }
        this.bearing = direction
    }

    turnRight () {
        this.bearing = DIRECTIONS[(DIRECTIONS.indexOf(this.bearing) + 1) % 4]
    }

    turnLeft () {
        this.bearing = DIRECTIONS[(DIRECTIONS.indexOf(this.bearing) + 3) % 4]
    }

    at (x, y) {
        this.coordinates = [x, y]
    }

    advance () {
        const setCoords = {
            'north': () => this.coordinates[1]++,
            'south': () => this.coordinates[1]--,
            'east':  () => this.coordinates[0]++,
            'west':  () => this.coordinates[0]--
          }
          setCoords[this.bearing].call(this)
    }

    instructions([...commands]) {
        return commands.map(instr => INSTRUCTIONS[instr])
    }

    place (loc) {
        this.orient(loc.direction)
        this.at(loc.x, loc.y)
    }

    evaluate (commands) {
        this.instructions([...commands]).forEach(command => this[command].call(this))
    }
}