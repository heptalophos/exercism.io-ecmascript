const STUDENTS = ['Alice', 'Bob', 'Charlie', 'David', 
                  'Eve', 'Fred', 'Ginny', 'Harriet', 
                  'Ileana', 'Joseph', 'Kincaid', 'Larry'];

const PLANTS = {'G': 'grass', 'C': 'clover', 'R': 'radishes', 'V': 'violets'};

const pots = (student, students, row0, row1) => {
    const offset = students.indexOf(student) *  2
    const plants = row0.slice( offset, offset + 2 ) + row1.slice(  offset, offset + 2 )
    return [...plants].map(d => PLANTS[d])
}

export default class Garden {
    constructor (diagram, students) {
        this.students = students ? students.sort() : STUDENTS
        this.row0 = diagram.split('\n')[0]
        this.row1 = diagram.split('\n')[1]
        this.students.forEach(student => {
            this[student.toLowerCase()] = pots(student, this.students, this.row0, this.row1)
        })
    }
}