function *range (start, end) {
    if (start > end) return
    yield start
    yield *range(start += 1, end)
}

const isPrime = (num) => [...range(2, num / 2)].filter(factor => num % factor === 0).length === 0

export default class Sieve {

    constructor (num) {
        this.upTo = num
        this.primes = [...range(2, this.upTo)].filter(n => isPrime(n))
    }
}