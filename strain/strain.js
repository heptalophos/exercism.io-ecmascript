
const Strain = {

    strain: (list, predicate, keeping=true) => {
        const keeps = [] 
        const discards = []
        list.forEach (x => {
            if (predicate(x)) {
                keeps.push(x)
            } else {
                discards.push(x)
            }
        })
        return (keeping) ? keeps : discards
    },

        keep : (list, predicate) => Strain.strain(list, predicate, true),
        discard : (list, predicate) => Strain.strain(list, predicate, false)
}

export default Strain