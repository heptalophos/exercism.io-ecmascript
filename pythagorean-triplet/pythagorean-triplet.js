export default class Triplet {

    constructor (a, b, c) {
        this.a = a
        this.b = b
        this.c = c
        return {
            sum :   () => this.a + this.b + this.c,
            product :   () => this.a * this.b * this.c,
            isPythagorean : () => this.a ** 2 + this.b ** 2 === this.c ** 2
        }
    } 

    static where ({maxFactor, minFactor = 1, sum}) {
        const target = sum
        let pythagoreans = []
        for (let x = minFactor; x <= maxFactor; x++) {
            for (let y = x + 1; y <= maxFactor; y++) {
                for (let z = y + 1; z <= maxFactor; z++) {
                    (x ** 2 + y ** 2 === z ** 2) ? pythagoreans.push(new Triplet(x, y, z)) : {}
                }
            }
        }
        const isTarget = (triplet) => triplet.sum() === target
        target ? pythagoreans = pythagoreans.filter(isTarget) : {}
        return pythagoreans
    }
}