const TRIANGLE_TYPE = { 1: 'equilateral', 2: 'isosceles', 3: 'scalene' }


export default class Triangle {

    constructor (...sides) {
        const order = (x, y)  => x - y
        this.sides = sides.sort(order)
    }

    kind () {
        this.throwSomething()
        const count = (new Set(this.sides)).size
        return TRIANGLE_TYPE[count]
    }

    throwSomething() {
        if (this.sides.some(side => side <= 0 ) || this.sides[2] > (this.sides[0] + this.sides[1])) {
            throw new Error ('Illegal')
        }
    }
}