export default class PhoneNumber {

    constructor (phoneNumber) {
        this._number = this.clean(phoneNumber)
    }

    number () {
        return this._number
    }

    clean (num) {
        if (num.match(/[a-z]+/)) {
            return null
        } 
        num = num.replace(/\D+/g, '') 
        return (num.length === 10) ? num : (num.length === 11 && num[0] === '1') ? num.slice(1) : null
    }
}
