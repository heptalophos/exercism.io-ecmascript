const pigLatin = (word) => {
    const [_, prefix, rest] = word.match(/^([^auoie]*qu|[^auoie]*)(.+)$/)
    const pig = (prefix.length === 0) ? `${word}ay` : `${rest}${prefix}ay`
    return pig
}

export default class PigLatin {

    translate (word) {
        return word
              .split(' ')
              .map(pigLatin)
              .join(' ')
    }
}