
const isSorted = (arr) => arr.every((item, idx) => idx === 0 || item >= arr[idx - 1])

export default class BinarySearch {

    constructor (arr) {
        if (isSorted(arr)) this.array = arr
    }

    indexOf(searched, lower=0, upper=this.array.length-1) {
        const cursor = lower + Math.ceil((upper - lower) / 2)
        const elem = this.array[cursor]
        if (elem === searched) return cursor
        else if (upper === lower && elem !== searched) return -1
        elem > searched ? upper = cursor : lower = cursor
        return this.indexOf(searched, lower, upper)
    }
} 