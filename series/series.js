export default class Series {

    constructor (numStr) {
        this.digits = [...numStr].map (digit => Number(digit))
    }

    slices (size) {
        if (size > this.digits.length) {
            throw new Error ('Slice size is too big.')
        } 
        return this.digits.slice(0, this.digits.length - size + 1)
               .map ((d, i) => (this.digits.slice(i, i + size)))
    }
}