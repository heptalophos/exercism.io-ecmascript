export const bufferEmptyException = () => new Error('Empty Buffer')
export const bufferFullException = () => new Error('Buffer Full')

const circularBuffer = (size) => {

    let buff = []
    let maxB = size
    const isValid = (val) => { return val !== undefined && val !== null}

    return {

        read : () => {
            if (buff.length !== 0) {
                return buff.splice(0, 1)[0]
            } else {
                throw bufferEmptyException()
            }
        },

        write : (val) => {
            if (val) {
                if (buff.length === maxB ) { 
                    throw bufferFullException() 
                } else {
                    if (isValid(val)) buff.push(val)
                }
            }
        },
        
        forceWrite : (val) => {
            if (isValid(val)) {
                if (buff.length === maxB) {
                    buff.splice(0, 1)[0]
                }
                buff.push(val) 
            }
        },
        
        clear : () => buff.length = 0 
    }
} 

export default circularBuffer