export  default class Binary {

    constructor (binary) {

        this.binary = binary.match(/^[01]+$/) ? parseInt(binary, 2) : 0

    }

    toDecimal () {

        return Number(this.binary.toString(10))
                
    }
}