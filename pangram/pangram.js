const notW = /[^a-zA-Z]+/g, 
      lenAll = 26

 export default class Pangram {

    constructor (text) {
        let justText = (text.replace(notW, '')).toLowerCase()
        this.unique = new Set([...justText])
    }

    isPangram() {
        return this.unique.size === lenAll
    }
 }

