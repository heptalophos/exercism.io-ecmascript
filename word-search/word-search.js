
const searchHorizontal = ({word, grid}) => {

    let rowIdx = 0, 
        startCol, 
        start, 
        end

    const getCoordinates = () => [[rowIdx+1, startCol], [rowIdx+1, startCol+word.length-1]]
    const getStartCol = (word) => grid[rowIdx].indexOf(word) + 1
    
    while (rowIdx < grid.length) {
        startCol = getStartCol(word)
        if (startCol) {
            [start, end] = getCoordinates()
        } else {
            startCol = getStartCol([...word].reverse().join(''))
            if (startCol) {
                [end, start] = getCoordinates()
            }
        }
        if (start && end) {
            return {start, end}
        }
        rowIdx ++
    }
}

const searchDiagonal = ({word, grid, isReversed = false, fromTop = true}) => {
    let rInc = fromTop ? 1 : -1
    let startRow = fromTop ? 0 : grid.length - 1
    let stopRow = fromTop ? ((r) => r < grid.length) : ((r) => r > 0)
    let diagFind = fromTop ? findDiagTopDown : findDiagBottomUp 

    for (let r = startRow ; (stopRow) (r); r += rInc ) {
        for (let c = 0; c < grid[r].length; c++) {
            const possibleCoords = diagFind(r, c, word, grid) 
            if (possibleCoords) {
                return formatCoordinates(possibleCoords, isReversed)
            }
        }
    }
    if (!isReversed) {
        const reversedWord = [...word].reverse().join('')
        return searchDiagonal({word: reversedWord, grid, isReversed: true, fromTop})
    }
}


const formatCoordinates = (coords, isReversed) => {
    return {
        true: {
            start: coords.end,
            end:   coords.start
        },
        false : coords,
    } [isReversed]
}


const flipCoordinates = (coords) => {
    if (!coords) {
        return undefined
    }
    return {
        start: coords.start.reverse(),
        end:   coords.end.reverse()
    }
}

const flipGrid = (grid) => {
    return [...grid[0]]
        .map ((col, c) => grid.map((row, r) => grid[r][c]))
        .map (row => row.join(''))
}


const findDiagTopDown = (r, c, word, grid) => {
    const outOfRange = (r, c, words, columns, letters) => {
        return r > columns - words + letters || c > columns - words + letters
    }

    const buildCoords = (startR, startC, r, c) => {
        return {
            start: [startR, startC], 
            end:   [r + 1, c]
        }
    }
    return findDiag (r, c, word, grid, 1, outOfRange, buildCoords)
}

const findDiagBottomUp = (r, c, word, grid) => {
    const outOfRange = (r, c, words, columns, letters) => {
        return r < words - letters - 1 || c > columns - words + letters
    }

    const buildCoords = (startR, startC, r, c) => {
        return {
            start: [startR, startC], 
            end:   [r + 1, c]
        }
    }
    return findDiag (r, c, word, grid, -1, outOfRange, buildCoords)
}


const findDiag = (r, c, word, grid, rInc, outOfRange, buildCoords) => {

    let foundLetters = ""
    let startR = r + 1
    let startC = c + 1
    for (let letter of word) {
        if (outOfRange(r, c, word.length, grid[r].length, foundLetters.length)) {
            return undefined
        }
        let foundLetter = grid[r].charAt(c++)
        if (foundLetter !== letter) {
            return undefined
        }
        foundLetters += foundLetter
        if (foundLetters === word) { 
            return buildCoords(startR, startC, r, c)
        }
        r += rInc
    }
}

const findAnyWord = (word, grid) => {
    return searchHorizontal({word, grid}) 
        || flipCoordinates(searchHorizontal({word, grid: flipGrid(grid)})) 
        || searchDiagonal({word, grid})
        || searchDiagonal({word, grid, fromTop: false})
}



export default class WordSearch {

    constructor (grid) {
        this.grid = grid
    }

    find (words) {
        return words
            .map(word => ({[word]: findAnyWord(word, this.grid)}))
            .reduce((acc, word) => Object.assign(acc, word), {})
    }
    
}


