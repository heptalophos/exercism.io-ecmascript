const used = {}

const randomAlpha = () => {
    let first = Math.floor(Math.random() * 26) + 65
    let second = Math.floor(Math.random() * 26) + 65
    return String.fromCharCode(first) + String.fromCharCode(second)
}

const randomNumeric = () => {
    let numeric = Math.floor(Math.random() * 1000)
    return numeric.toString().padStart(3, 0)
}

const generateName = () => {
    let name =  randomAlpha() + randomNumeric()
    if (used[name]) {
        name = generateName()
    } else {
        used[name] = true
    }
    return name
}


export default class robot {

    constructor () {
        this._name = generateName()
    }

    get name () {
        return this._name
    }

    reset () {
        this._name = generateName()
    }
}