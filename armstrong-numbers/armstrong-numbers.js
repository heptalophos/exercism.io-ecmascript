export default class ArmstrongNumber {

    static validate (number) {
        const digits = [...String(number)]
        const sum = digits.reduce((acc, n) => (
            acc + Math.pow(n, digits.length)
        ), 0)
        return sum == number
    }

}