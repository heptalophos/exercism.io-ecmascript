const assert = require('assert')

const isPrime = num => {
    for (let i = 2; i < num; i++) {
        if (num % i === 0) { 
            return false
        }
    }
    return num >= 2
}

export default class DiffieHellman {

    constructor (p, g) {
        assert (isPrime(p), "p is not prime")
        assert (isPrime(g), 'g is not prime')
        assert (p > 2 && g > 2, 'p or g out of range')
        this.p = p
        this.g = g 
    }

    getPublicKeyFromPrivateKey (privateKey) {
        assert (privateKey > 1 && privateKey < this.p, 'private key must be greater than 1 and less than p')
        return this.g ** privateKey % this.p
    }

    getSharedSecret (privateKey, publicKey) {
        return publicKey ** privateKey % this.p
    }
}