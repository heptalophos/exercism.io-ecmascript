export default (list, op) => {
    const acc = []
    list.forEach( x => acc.push(op(x)))
    return acc
}