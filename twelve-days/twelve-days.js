const NTHDAY = [
    'first',
    'second',
    'third',
    'fourth',
    'fifth',
    'sixth',
    'seventh',
    'eighth',
    'ninth',
    'tenth',
    'eleventh',
    'twelfth'
  ]
  
const GIFTS = [
    'twelve Drummers Drumming',
    'eleven Pipers Piping',
    'ten Lords-a-Leaping',
    'nine Ladies Dancing',
    'eight Maids-a-Milking',
    'seven Swans-a-Swimming',
    'six Geese-a-Laying',
    'five Gold Rings',
    'four Calling Birds',
    'three French Hens',
    'two Turtle Doves',
    'and a Partridge in a Pear Tree.\n'
  ]
  
const VERSESTART = 'On the __nth__ day of Christmas my true love gave to me'

class TwelveDays {

    verse (from, to = 0) {
        if (!to) return this._v(from)
        let output = []
        for (let i = from; i <= to; i++) {
            output.push(this._v(i))
        }
        return output.join('\n')
    }

    sing () {
        return this.verse(1, 12)
    }

    _v (n) {
        if (n < 1) throw 'Number is less than 1'
        let output = []
        for (let i = 0; i < n; i++) {
            let gift = GIFTS[GIFTS.length - 1 - i]
            if (n === 1) gift = gift.replace(/^and /, '')
            output.unshift(gift)
        }
        output.unshift(VERSESTART.replace('__nth__', NTHDAY[n - 1]))
        return output.join(', ')
    }
}

// export default TwelveDays
module.exports = TwelveDays