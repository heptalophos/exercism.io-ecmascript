
export default class Allergies {

    constructor (allergy) {
        this.allergy = allergy
        this.list = () =>["eggs","peanuts","shellfish","strawberries","tomatoes","chocolate","pollen","cats"]
                         .filter ((allergy, i) => this.allergy & 2**i)
    }

    allergicTo (allergen) {
        return this.list().some(a => a == allergen)
    }

}