
const deepcopy = (x) => JSON.parse(JSON.stringify(x))

export default class School {

    constructor () {
        this.db = {}
    }

    roster () {
        return deepcopy(this.db)
    }

    grade (grade) {
        // return (this.db[grade] !== undefined) ? this.sort(grade) : []
        return deepcopy(this.db[grade] || [])
    }

    add (student, grade) {
        const students = this.grade(grade)
        // this.db[grade] = Object.freeze(this.grade(grade).concat(student).sort())
        this.db[grade] = students
        students.push(student)
        students.sort()
    }

}