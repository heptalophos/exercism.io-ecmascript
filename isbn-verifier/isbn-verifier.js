export default class ISBN {

    constructor (isbn) {
        this.digits = [...isbn.replace(/-/g, '')]
                      .map ( d => (d === 'X') ? 10 : (d.match(/\d/)) ? parseInt (d, 10) : NaN)
    }

    isValid () {
        return ((this.digits.map((d, i) => d * (10 - i))).reduce((acc, d) => acc + d, 0)) % 11 === 0
    }
}