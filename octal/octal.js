export default class Octal {

    constructor (str) {
        this.octal = [...str].reverse().map(Number)
    }

    toDecimal () {
        if (!(/^[01234567]+$/.test(this.octal.join('')))) {
            return 0
          }
        return this.octal.reduce((acc, dig, i) => acc += dig * Math.pow(8, i), 0)
    }
}