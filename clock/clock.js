
const padit = (num) => num < 10 ? '0' + num : num.toString()

const at = (hour, min) => new Clock(hour, min)

class Clock {

    constructor (hour=0, min=0) {
        hour += Math.floor (min / 60)
        hour = hour % 24
        min  = min % 60
        this.hour = (hour < 0) ? hour + 24 : hour
        this.min  = (min < 0) ? min + 60 : min
    }

    formatTime() {
        return `${padit (this.hour)}:${padit(this.min)}` 
    }

    toString() { 
        return this.formatTime()
    }

    plus (mins = 0) {
        return at(this.hour, this.min + mins)
    }

    minus (mins = 0) {
        return at(this.hour, this.min - mins)
    }

    equals (clock) {
        return this.formatTime() === clock.toString()
    }
}

export default at