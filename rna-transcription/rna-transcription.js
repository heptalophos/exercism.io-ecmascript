const TRANSL = {
    G: 'C',
    C: 'G',
    T: 'A',
    A: 'U',
  };

export default class Transcriptor {

    constructor() {

    }
    
    toRna(dna) {
        if (/[^ACGT]/.test(dna))
            throw new Error('Invalid input DNA.') 
        else 
            return dna.split('').map((n) => TRANSL[n]).join('')    
    }

}