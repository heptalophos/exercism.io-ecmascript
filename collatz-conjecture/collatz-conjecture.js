export default function steps (num)  {

    if (num <= 0) {
        throw new Error('Only positive numbers are allowed')
    }

    const iter = (n, s) => (n === 1) ? s : (n % 2 === 0) ? iter(n / 2, s + 1) : iter((3 * n) + 1, s + 1)

    return iter (num, 0)

}