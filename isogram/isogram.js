export default class Isogram {

    constructor (word) {
        this.word = word.replace(/\s|-/g, '').toLowerCase()
        this.len = this.word.length
    }

    isIsogram () {
        return this.word.split('').filter((c, i, self) => self.indexOf(c) === i).length === this.len
    }
}