export default class Hexadecimal {

    constructor (hexNum) {
        this.hexadecimal = hexNum.toLowerCase().match(/[0-9a-f]+/g)[0]
        this.hexadecimal.length !== hexNum.length ? this.hexadecimal = '' : {}
    }

    toDecimal () {
        return [...this.hexadecimal]
               .reduce ((decimal, hexDigit) => decimal * 16 + '0123456789abcdef'.indexOf(hexDigit), 0)
    }
}