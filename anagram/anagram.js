const sorted = word => word.split('').sort().join('')

export default class Anagram {

    constructor (word) {
        this.word = word.toLowerCase()
    }

    matches (...words) {
        return words.reduce((anagrams, w) => anagrams.concat(w), [])
            .filter(w => ((w.toLowerCase() !== this.word) && (sorted(w.toLowerCase()) === sorted(this.word))))
    }

}