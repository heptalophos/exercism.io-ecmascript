
export default class Hamming {

    constructor () {}

    compute (dna0, dna1) {
        if (dna0.length === dna1.length) {
            return [...dna0].filter((nucl, i) => nucl !== dna1[i]).length
        } else {
            throw new Error("DNA strands must be of equal length.")
        }
    }
}