export default class Acronyms {

    constructor () {}

    static parse (sentence) {
        return sentence.match(/[A-Z]+[a-z]*|[a-z]+/g).reduce((acro, word) => acro += word[0].toUpperCase(), '')
    }
}