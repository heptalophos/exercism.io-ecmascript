export class ArgumentError extends Error {
    constructor () {
        super()
    }
}

const REGEX = /([a-z\s]+?)\s+(-?\d+)/g;

const OPS = {
    'what is':        (x, y) => y,
    'plus':           (x, y) => x + y,
    'minus':          (x, y) => x - y, 
    'multiplied by':  (x, y) => x * y,
    'divided by':     (x, y) => x / y,
    // 'raised to the ': (x, y) => x ^ y
}

export class WordProblem {

    constructor (question) {
        this.question = question
    }

    answer () {
        const question = this.question.toLowerCase()
        if (!/(\d+)\?$/.test(question)) {
            throw new ArgumentError()
        }
        let res, matches
        while ((matches = REGEX.exec(question)) !== null) {
            const op = OPS[matches[1].trim()] 
            if (!op) {
                throw new ArgumentError()
            }
            res = op(res, parseFloat(matches[2]))
        } 
        return res
    }

}