
export default class Words {

    constructor () {}

    count (sentence) {
        return sentence.trim()
                       .toLowerCase()
                       .split(/\s+/)
                       .reduce((nums, word) => {
                            // nums[word] = (nums[word]) ? ++nums[word] : 1
                            ++nums[word] || (nums[word] = 1)
                            return nums
                        }, {})
    }
}