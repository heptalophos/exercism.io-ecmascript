class Node {
    constructor(data, prev, next) {
      this.data = data;
      this.prev = prev;
      this.next = next;
    }
  }
    
export default class LinkedList {
    constructor() {
      this.head = undefined;
      this.tail = undefined;
      this.length = 0;
    }
  
    push(data) {
      const newNode = new Node(data, this.tail);
      if (this.tail) {
        this.tail.next = newNode;
      }
      this.tail = newNode;
      if (!this.head) {
        this.head = newNode;
      }
      this.length += 1;
    }
  
    pop() {
      const tail = this.tail;
      if (tail) {
        const data = tail.data;
        if (tail.prev) {
          tail.prev.next = undefined;
        }
        if (this.tail === this.head) {
          this.head = tail.prev;
        }
        this.tail = tail.prev;
        this.length -= 1;
        return data;
      }
    }
  
    shift() {
      const head = this.head;
      if (head) {
        const data = head.data;
        if (head.next) {
          head.next.prev = undefined;
        }
        if (this.tail === this.head) {
          this.tail = head.prev;
        }
        this.head = head.next;
        this.length -= 1;
        return data;
      }
    }
  
    unshift(data) {
      const newNode = new Node(data, undefined, this.head);
      if (this.head) {
        this.head.prev = newNode;
      }
      this.head = newNode;
      if (!this.tail) {
        this.tail = newNode;
      }
      this.length += 1;
    }
  
    count() {
      return this.length;
    }
  
    'delete'(data) {
      let next = this.head;
      while (next) {
        if (next.data === data) {
          this.deleteNode(next);
          return;
        }
        next = next.next;
      }
    }
  
    deleteNode(node) {
      switch (node) {
        case this.head:
          this.shift();
          return;
        case this.tail:
          this.pop();
          return;
        default:
          node.prev.next = node.next;
          node.next.prev = node.prev;
          this.length -= 1;
          return;
      }
    }
}