
export default class Trinary {

    constructor (str) {
        this.trinary = str.match(/^[012]+$/) ? parseInt(str, 3) : 0
    }

    toDecimal () {
        return Number(this.trinary.toString(10))
    }
}