
const alphabet = [...'abcdefghijklmnopqrstuvwxyz']
const cipher = [...alphabet].reverse()
const chunk = (str) => str.match(/.{1,5}/g).join(' ');


export default {
   
    encode : (str) => chunk([...str.toLowerCase().replace(/[^\da-z]/g, '')]
                            .map(x => (Number(x)) ? x : cipher[alphabet.indexOf(x)])
                            .join(''))
                     
}