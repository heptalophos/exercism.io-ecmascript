export default class Series {

    constructor(series) {
        if (series.match('[^0-9]'))
            throw new Error('Invalid input.')
        this.digits = [...series].map(d => +d)
    }

    partition(size) {
        if (size > this.digits.length) {
            throw new Error('Slice size is too big.')
        }
        return this.digits.slice(0, this.digits.length - size + 1)
              .map((_, i) => this.digits.slice(i, i + size))
    }

    largestProduct (size) {
        if (size < 0) {
            throw new Error('Invalid input.')
        }
        let prod = 0
        let max = (size === 0) ? 1 : 0
        this.partition(size).forEach((span) => {
            prod = span.reduce((a, c) => a * c, 1)
            if (prod > max) max = prod
        })
        return max
    }
} 

