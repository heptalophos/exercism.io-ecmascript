export default (factors = [3, 5]) => {

    const nums = factors.filter((elem, i) => factors.indexOf(elem) === i).sort((x, y) => x - y)

    return {
        to: limit => {
            let sum = 0;
            let added = -1

            for (let i = factors[0]; i < limit; i++) {
                for (let j = 0; j < factors.length; j++) {
                    if (added !== i && !(i % factors[j])) {
                        sum += i
                        added = i
                        }   
                    }
                }
            return sum
        } 
    }
}

