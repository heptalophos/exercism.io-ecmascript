/* eslint-disable no-unused-vars */
//
// This is only a SKELETON file for the 'Bob' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

class Bob {

  constructor () {}
  
  hey(message) {
  
    const shout = message === message.toUpperCase() && (/[A-Z]/).test(message)
    const question = message.endsWith('?')
    const silent = !message.trim()
    const forceful = shout && question
    
    if (forceful) {
      return "Calm down, I know what I'm doing!"
    }
    else if (shout) {
      return 'Whoa, chill out!'
    }
    else if (question) {
      return 'Sure.'
    } 
    else if (silent) {
      return 'Fine. Be that way!'
    }
    else {
      return 'Whatever.'
    }
  }
}

export default Bob;

