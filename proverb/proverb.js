const proverb = (...words) => {
    if (words.length < 1) {
        return ""
    }
    let last = words[words.length - 1]
    let result = []
    let upTo = last.qualifier ? words.length - 1 : words.length
    for (let i = 1; i < upTo; i++) {
        result.push(`For want of a ${words[i-1]} the ${words[i]} was lost.\n`)
    }
    result.push(`And all for the want of a ${last.qualifier ? last.qualifier + ' ' : ''}${words[0]}.`)

    return result.join("")
}

export default proverb