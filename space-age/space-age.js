const ORBITALPERIOD = {
    Earth:   1,
    Mercury: 0.2408467,
    Venus:   0.61519726,
    Mars:    1.8808158,
    Jupiter: 11.862615,
    Saturn:  29.447498,
    Uranus:  84.016846,
    Neptune: 164.79132
}

export default class SpaceAge {

    constructor (seconds) {
        this.seconds = seconds
        this.yearsOnEarth = seconds / 31557600
    }

    onMercury () {
        return parseFloat((this.yearsOnEarth / ORBITALPERIOD['Mercury']).toFixed(2))
    }

    onVenus () {
        return parseFloat((this.yearsOnEarth / ORBITALPERIOD['Venus']).toFixed(2))
    }

    onEarth () {
        return parseFloat((this.yearsOnEarth / ORBITALPERIOD['Earth']).toFixed(2))
    }

    onMars () {
        return parseFloat((this.yearsOnEarth / ORBITALPERIOD['Mars']).toFixed(2))
    }

    onJupiter () {
        return parseFloat((this.yearsOnEarth / ORBITALPERIOD['Jupiter']).toFixed(2))
    }

    onSaturn () {
        return parseFloat((this.yearsOnEarth / ORBITALPERIOD['Saturn']).toFixed(2))
    }

    onUranus () {
        return parseFloat((this.yearsOnEarth / ORBITALPERIOD['Uranus']).toFixed(2))
    }

    onNeptune () {
        return parseFloat((this.yearsOnEarth / ORBITALPERIOD['Neptune']).toFixed(2))
    }

}