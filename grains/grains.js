import BigInt from './lib/big-integer'

export default class Grains {

    constructor () {
        
        this.square = n => BigInt(2).pow(n - 1).toString(),
        this.total  =  () => BigInt(2).pow(64).minus(1).toString()

    }
    
}