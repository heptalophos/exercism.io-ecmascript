
export default class PrimeFactors {

    constructor () {}

    for (number) {

        const pFactors = []
        let i = 2
        while (number > 1) {
            if (number % i === 0) {
                number /= i
                pFactors.push (i)
            } 
            else {
                i++
                if (i % 2 === 0) {
                    i++
                }
            }
        }
        return pFactors
    }
}